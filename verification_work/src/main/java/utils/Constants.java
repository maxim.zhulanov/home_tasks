package utils;

public final class Constants {

    public static final int EXIT = 0;

    public static final int ADD_SOUVENIR_AND_PRODUCER = 1;

    public static final int EDIT_PRODUCER = 2;

    public static final int EDIT_SOUVENIR = 3;

    public static final int FIND_ALL_PRODUCERS = 4;

    public static final int FIND_ALL_SOUVENIRS = 5;

    public static final int FIND_SOUVENIRS_IN_COUNTRY = 6;

    public static final int FIND_PRODUCERS_PRICES_LESS_THAN = 7;

    public static final int FIND_PRODUCERS_AND_THEIRS_SOUVENIRS = 8;

    public static final int FIND_SOUVENIR_IN_YEAR = 9;

    public static final int FIND_SOUVENIRS_FOR_EACH_YEAR = 10;

    public static final int DELETE_PRODUCER_AND_HIS_SOUVENIRS = 11;

    private Constants() {
        throw new UnsupportedOperationException();
    }
}