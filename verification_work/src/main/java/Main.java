import model.Producer;
import model.Souvenir;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import service.SouvenirService;
import service.impl.CSVProcessor;
import service.impl.SouvenirServiceImpl;
import utils.Menu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Predicate;

import static utils.Constants.*;

public class Main {

    private static final String INPUT_OPTION = "i";

    private static final String DELIMITER_OPTION = "d";

    private static final SouvenirService souvenirService = new SouvenirServiceImpl();

    public static void main(String[] args) {
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(getOptions(), args);
            char specifiedDelimiter = getDelimiter(cmd);

            try (InputStream in = resolveInputStream(cmd)) {
                CSVProcessor csvProcessor = new CSVProcessor();
                List<Souvenir> souvenirs = csvProcessor.readSouvenirsFromFile(in, specifiedDelimiter);

                Menu menu = new Menu();
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    switch (menu.menuChooseMode()) {
                        case EXIT:
                            return;

                        case ADD_SOUVENIR_AND_PRODUCER:
                            Souvenir newSouvenir;
                            System.out.println("Enter souvenir name:");
                            String souvenirName = scanner.nextLine();
                            System.out.println("Enter producer name:");
                            String pName = scanner.nextLine();
                            System.out.println("Enter souvenir price(separated by ,):");
                            Double souvenirPrice = scanner.nextDouble();

                            Optional<Producer> any = souvenirService.findAllProducers(souvenirs).stream()
                                    .filter(p -> p.getName().equals(pName))
                                    .findAny();
                            if (any.isPresent()) {
                                newSouvenir = new Souvenir(souvenirName, any.get(), addDate(), souvenirPrice);
                            } else {
                                System.out.println("Enter producer country:");
                                String pCountry = scanner.next();
                                newSouvenir = new Souvenir(souvenirName, new Producer(pName, pCountry), addDate(), souvenirPrice);
                            }
                            if (newSouvenir.getCreatedAt().compareTo(LocalDate.now()) > 0) {
                                System.out.println("You can't enter a date from the future!");
                                Thread.sleep(3000);
                                break;
                            }
                            souvenirs.add(newSouvenir);
                            csvProcessor.writeSouvenirsToFile(args[1], souvenirs, specifiedDelimiter);
                            break;
                        case EDIT_PRODUCER:
                            System.out.println("Enter producer name:");
                            String name = scanner.next();
                            System.out.println("Enter producer country");
                            String country = scanner.next();
                            Predicate <Producer> producerNamePredicate = pr -> pr.getName().equals(name);
                            Predicate <Producer> producerCountryPredicate = pr -> pr.getCountry().equals(country);
                            Optional<Producer> anyProducerWithThatName = souvenirs.stream().map(Souvenir::getProducer)
                                    .filter(producerNamePredicate.and(producerCountryPredicate))
                                    .findAny();
                            if (!anyProducerWithThatName.isPresent()) {
                                System.out.println("No such element!!!\n");
                                Thread.sleep(3000);
                                break;
                            }
                            Producer producer = anyProducerWithThatName.get();
                            System.out.println("===Change country of producer===");
                            System.out.println("Enter new country for producer: " + producer.getName());
                            String newCountry = scanner.next();
                            souvenirService.editCountryOfProducer(souvenirs, producer.getName(), newCountry);
                            csvProcessor.writeSouvenirsToFile(args[1], souvenirs, specifiedDelimiter);
                            break;
                        case EDIT_SOUVENIR:
                            System.out.println("Enter producer name:");
                            String prName = scanner.next();
                            System.out.println("Enter souvenir name:");
                            String souvName = scanner.next();
                            System.out.println("Enter producer country");
                            String c = scanner.next();
                            Predicate<Souvenir> sN = s -> s.getName().equals(souvName);
                            Predicate<Souvenir> pN = s -> s.getProducer().getName().equals(prName);
                            Predicate <Souvenir> pC = s -> s.getProducer().getCountry().equals(c);
                            Predicate<Souvenir> souvenirPredicate = sN.and(pN).and(pC);
                            Optional<Souvenir> toEdit = souvenirs.stream()
                                    .filter(souvenirPredicate)
                                    .findAny();
                            if (!toEdit.isPresent()) {
                                System.out.println("No such element!!!\n");
                                Thread.sleep(3000);
                                break;
                            }
                            System.out.println("===Change price of souvenir===");
                            System.out.println("Enter a new price for souvenir: " + souvName);
                            double newPrice = scanner.nextDouble();
                            souvenirService.editPriceOfSouvenir(souvenirs, souvenirPredicate, newPrice);
                            csvProcessor.writeSouvenirsToFile(args[1], souvenirs, specifiedDelimiter);
                            break;
                        case FIND_ALL_PRODUCERS:
                            souvenirService.findAllProducers(souvenirs).forEach(System.out::println);
                            break;
                        case FIND_ALL_SOUVENIRS:
                            souvenirService.findAllSouvenirs(souvenirs).forEach(System.out::println);
                            break;
                        case FIND_SOUVENIRS_IN_COUNTRY:
                            System.out.println("Enter country:");
                            String ctr = scanner.next();
                            souvenirService.findSouvenirsByCountry(souvenirs, ctr).forEach(System.out::println);
                            break;
                        case FIND_PRODUCERS_PRICES_LESS_THAN:
                            System.out.println("Enter a specified price:");
                            Double price = scanner.nextDouble();
                            souvenirService.findProducersByPriceLessThan(souvenirs, price).forEach(System.out::println);
                            break;
                        case FIND_PRODUCERS_AND_THEIRS_SOUVENIRS:
                            Map<Producer, List<Souvenir>> allInformationProducerAndHisSouvenirs = souvenirService.findAllInformationProducerAndHisSouvenirs(souvenirs);
                            printMap(allInformationProducerAndHisSouvenirs);
                            break;
                        case FIND_SOUVENIR_IN_YEAR:
                            System.out.println("Enter souvenir:");
                            String souvenir = scanner.next();
                            System.out.println("Enter year:");
                            int year = scanner.nextInt();
                            souvenirService.findProducersBySouvenirAndYear(souvenirs, souvenir, year).forEach(System.out::println);
                            break;
                        case FIND_SOUVENIRS_FOR_EACH_YEAR:
                            Map<Year, List<Souvenir>> souvenirsByYear = souvenirService.findSouvenirsByYears(souvenirs);
                            printMap(souvenirsByYear);
                            break;
                        case DELETE_PRODUCER_AND_HIS_SOUVENIRS:
                            System.out.println("Enter a producer name:");
                            String prodName = scanner.next();
                            System.out.println("Enter producer country:");
                            String countryName = scanner.next();
                            souvenirService.deleteProducerAndHisSouvenirs(souvenirs, new Producer(prodName, countryName));
                            csvProcessor.writeSouvenirsToFile(args[1], souvenirs, specifiedDelimiter);
                            break;
                        default:
                            System.out.println("Illegal option. Please try again");
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            HelpFormatter helper = new HelpFormatter();
            helper.printHelp("Usage:", getOptions());
        }
    }

    private static char getDelimiter(CommandLine cmd) {
        char delimiter = ',';
        if (cmd.hasOption(DELIMITER_OPTION)) {
            delimiter = cmd.getOptionValue(DELIMITER_OPTION).charAt(0);
        }
        return delimiter;
    }

    private static Options getOptions() {
        Options options = new Options();
        options.addOption(Option.builder(INPUT_OPTION)
                .longOpt("input")
                .hasArg()
                .required(true)
                .desc("set input csv file")
                .build());

        options.addOption(Option.builder(DELIMITER_OPTION)
                .longOpt("delimiter")
                .hasArg()
                .required(false)
                .desc("specify the delimiter character in CSV file")
                .build());
        return options;
    }

    private static InputStream resolveInputStream(CommandLine cmd) throws FileNotFoundException {
        return new FileInputStream(cmd.getOptionValue(INPUT_OPTION));
    }

    private static <T> void printMap(Map<T, List<Souvenir>> map) {
        TreeMap<T, List<Souvenir>> producerListTreeMap = new TreeMap<>(map);
        Set<T> keys = producerListTreeMap.keySet();
        for (T key : keys) {
            System.out.println(key + " | ==> |: " + map.get(key));
        }
    }

    private static LocalDate addDate() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a date like [yyyy-MM-dd]: ");
        String str = scan.nextLine();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(str, dtf);
    }
}