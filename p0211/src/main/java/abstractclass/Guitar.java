package abstractclass;

public class Guitar extends Instrument{
    @Override
    protected void playNote(char note) {
        System.out.println("Guitar play note: " + note);
    }
}
