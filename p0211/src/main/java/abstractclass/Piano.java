package abstractclass;

public class Piano extends Instrument {
    @Override
    protected void playNote(char note) {
        System.out.println("Piano play note: " + note);
    }
}
