package abstractclass;

public class Main {
    public static void main(String[] args) {
        Instrument drum = new Drum();
        Instrument guitar = new Guitar();
        Instrument piano = new Piano();
        Instrument violin = new Violin();

        drum.play("bktcsgdg");
        guitar.play("brabrabru");
        piano.play("tyrynayo");
        violin.play("iyiuiiyi");
    }
}
