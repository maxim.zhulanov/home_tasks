package delegation;

public enum FilmType {
    NEW_FILM(30, 0.25), REGULAR(20, 0), CHILDREN(10, 0.2);

    private final int price_multiplier;
    private final double bonus_multiplier;

    FilmType(int price_multiplier, double bonus_multiplier) {
        this.price_multiplier = price_multiplier;
        this.bonus_multiplier = bonus_multiplier;
    }

    public int getPrice(int days) {
        return days * price_multiplier;
    }

    public double getBonus(int days) {
        return (days * price_multiplier) * bonus_multiplier;
    }
}
