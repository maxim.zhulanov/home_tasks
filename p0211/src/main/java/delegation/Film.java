package delegation;

public class Film {

    private FilmType type = FilmType.NEW_FILM;
    private int days = 1;

    public Film(int days) {
        this.days = days;
    }

    public Film() {
    }

    public void setType(FilmType type) {
        this.type = type;
    }

    public void price(){
        System.out.println("Price: " + type.getPrice(days));
    }
    public void bonus() {
        System.out.println("Bonus: " + type.getBonus(days));
    }
}
