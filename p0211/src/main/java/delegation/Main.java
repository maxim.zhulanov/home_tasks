package delegation;

public class Main {
    public static void main(String[] args) {
        Film film_1 = new Film(2);
        film_1.price();
        film_1.bonus();
        System.out.println();

        film_1.setType(FilmType.REGULAR);
        film_1.price();
        film_1.bonus();
        System.out.println();

        film_1.setType(FilmType.CHILDREN);
        film_1.price();
        film_1.bonus();
    }
}
