package host;

import broadcast.Broadcast;

import java.util.Arrays;
import java.util.List;

public class Host {
    private String name;
    private int experience;
    private String CV;
    private String broadcasts;

    public static class HostBuilder {
        private String name;
        private int experience;
        private String CV;
        private String broadcasts = "";

        public HostBuilder name(String name) {
            this.name = name;
            return this;
        }

        public HostBuilder experience(int experience) {
            this.experience = experience;
            return this;
        }

        public HostBuilder CV(String CV) {
            this.CV = CV;
            return this;
        }

        public HostBuilder broadcasts(String broadcasts) {
            this.broadcasts = broadcasts;
            return this;
        }

        public Host build() {
            Host host = new Host();
            host.name = name;
            host.experience = experience;
            host.CV = CV;
            host.broadcasts = broadcasts;
            return host;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getCV() {
        return CV;
    }

    public void setCV(String CV) {
        this.CV = CV;
    }

    public String getBroadcasts() {
        return broadcasts;
    }

    public void setBroadcasts(String broadcasts) {
        this.broadcasts = broadcasts;
    }

    public void addBroadcastTitle(String broadcastTitle) {
        broadcasts = broadcasts + " " + broadcastTitle;
    }

    @Override
    public String toString() {
        return "Host{" +
                "name='" + name + '\'' +
                ", experience(years)=" + experience +
                ", CV='" + CV + '\'' +
                ", broadcasts=" + broadcasts +
                '}';
    }
}
