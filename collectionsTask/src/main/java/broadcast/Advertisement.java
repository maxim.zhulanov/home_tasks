package broadcast;

public class Advertisement implements BroadcastPart{
    private String title;
    private final int price = 5; //5 за секунду
    private int duration;

    public Advertisement(String title, int duration) {
        this.title = title;
        this.duration = duration;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public double getIncome() {
        return getDuration() * price;
    }

    @Override
    public String toString() {
        return "Advertisement{" +
                "title='" + title + '\'' +
                ", price(for second)=" + price +
                ", duration(in seconds)=" + duration +
                '}';
    }
}