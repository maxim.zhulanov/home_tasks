package broadcast;

public class Interview implements BroadcastPart{
    private String respondent;
    private final double price = 0.5; // 30 за минуту -> 30/60 секунд = 0.5 за секунду
    private int duration;

    public Interview(String respondent, int duration) {
        this.respondent = respondent;
        this.duration = duration;
    }

    public String getRespondent() {
        return respondent;
    }

    public void setRespondent(String respondent) {
        this.respondent = respondent;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public double getIncome() {
        return getDuration() * price;
    }

    @Override
    public String toString() {
        return "Interview{" +
                "respondent='" + respondent + '\'' +
                ", price(for second)=" + price +
                ", duration(in seconds)=" + duration +
                '}';
    }
}
