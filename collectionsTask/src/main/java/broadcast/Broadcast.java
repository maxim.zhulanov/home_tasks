package broadcast;

import host.Host;

import java.util.ArrayList;
import java.util.List;

public class Broadcast {
    private String title;
    private int maxDuration;
    private List<BroadcastPart> broadcastParts = new ArrayList<>();
    private List<Host> hosts = new ArrayList<>();

    public Broadcast(String title, int maxLength) {
        this.title = title;
        this.maxDuration = maxLength;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public List<BroadcastPart> getBroadcast() {
        return broadcastParts;
    }

    public void setBroadcast(List<BroadcastPart> broadcastParts) {
        this.broadcastParts = broadcastParts;
    }

    public List<Host> getHosts() {
        return hosts;
    }

    public void setHosts(List<Host> hosts) {
        this.hosts = hosts;
    }

    public void addHost(Host newHost) {
        this.hosts.add(newHost);
        newHost.addBroadcastTitle(title);
    }

    public void addBroadcastPart(BroadcastPart broadcastPart) throws Exception {
        if(getBroadcastDuration() + broadcastPart.getDuration() > maxDuration) {
            throw new Exception("Broadcast time is limited. This part will not fit in the broadcast");
        }
        else if(broadcastPart.getIncome() != 0) {
            if(getDurationOfPaidContent() > (getBroadcastDuration() / 2)) {
                System.out.println(getBroadcastDuration() + " i " + getDurationOfPaidContent());
                throw new Exception("The duration of paid content should be less than 50% of all broadcast duration");
            } else {
                broadcastParts.add(broadcastPart);
            }
        }
        else {
            broadcastParts.add(broadcastPart);
        }
    }

    public int getBroadcastDuration() {
        return broadcastParts.stream()
                .mapToInt(BroadcastPart::getDuration)
                .sum();
    }

    public int getDurationOfPaidContent() {
        return broadcastParts.stream()
                .filter(broadcastPart -> broadcastPart.getIncome() != 0)
                .mapToInt(BroadcastPart::getDuration)
                .sum();
    }

    public double getBroadcastIncome() {
        return broadcastParts.stream()
                .mapToDouble(BroadcastPart::getIncome)
                .sum();
    }

    public void getInfo() {
        System.out.println("Broadcast -> " + title + ". Duration(in seconds): " + getBroadcastDuration() + ". Income: " + getBroadcastIncome() + "" +
                "\n Host(-s): ");
        hosts.stream().map(Host::getName).forEach(x -> System.out.println(x + ". "));
        System.out.println("Broadcast parts: ");
        broadcastParts.forEach(System.out::println);
    }
}
