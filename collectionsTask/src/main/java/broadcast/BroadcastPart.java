package broadcast;

public interface BroadcastPart {
    double getIncome();
    int getDuration();
    void setDuration(int duration);
}
