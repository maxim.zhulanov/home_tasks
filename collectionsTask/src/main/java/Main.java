import broadcast.Advertisement;
import broadcast.Broadcast;
import broadcast.Interview;
import broadcast.Song;
import host.Host;

public class Main {
    public static void main(String []args) throws Exception {
        Host host_1 = new Host.HostBuilder()
                .name("Gennadiy Semenov")
                .experience(1)
                .build();

        Host host_2 = new Host.HostBuilder()
                .name("Evgeniy Naumenko")
                .CV("Host from Mykolaiyv. Intern, new novice host.")
                .build();

        Broadcast broadcast = new Broadcast("<Good morning, we are from Ukraine!>", 10800);

        broadcast.addHost(host_1);
        broadcast.addHost(host_2);

        broadcast.addBroadcastPart(new Song("La la la", "Shakira", 5000));
        broadcast.addBroadcastPart(new Advertisement("HeadAndShoulders", 120));
        broadcast.addBroadcastPart(new Interview("Maxim Zhulanov", 1800));

        System.out.println(host_2.getBroadcasts());

        broadcast.getInfo();

        Broadcast broadcast_2 = new Broadcast("<Good evening, we are from Ukraine!>", 10800);
        broadcast_2.addHost(host_1);
        broadcast_2.addBroadcastPart(new Song("Bam param", "Maksimiliano", 5000));
        broadcast_2.addBroadcastPart(new Advertisement("GeekForLess", 240));
        broadcast_2.addBroadcastPart(new Interview("Evgeniy Berkunskiy", 100));

        broadcast_2.getInfo();

        System.out.println(host_1.getBroadcasts());
    }
}
