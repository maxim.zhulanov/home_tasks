public class Main {

    public static void main(String[] args)
    {
        Calculator calculator  = new Calculator(-0.5, 2);

        int steps = calculator.numberOfSteps(); //steps
        double[][] x_y_Matrix = calculator.calculateMatrix();

        int id_highestY = calculator.getIdOfHighestY();  // id of the largest Y in the array
        int id_smallestY  = calculator.getIdOfLowestY();   // id of the smallest Y in the array

        System.out.println("Highest Y: " + calculator.getValuesByID(id_highestY));
        System.out.println("Lowest Y: " + calculator.getValuesByID(id_smallestY));

        System.out.println("Test #175: " + calculator.getValuesByID(174));
        System.out.println("Test #350: " + calculator.getValuesByID(349));
        System.out.println("Test #750: " + calculator.getValuesByID(749));

        calculator.printInfoAll();
    }
}
