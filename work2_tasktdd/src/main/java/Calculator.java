import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;

public class Calculator
{
    private final double min_X = 0.0;
    private final double max_X = 3.0;
    private final double stepIncrement = 0.004;

    private double a = 0.0; // variable a
    private double b = 0.0; // variable b

    private double[][] elementArray = new double[0][3];    //step, y, x

    public Calculator(double a, double b)
    {
        this.a = a;
        this.b = b;
    }

    private double function(double x)
    {
        double y = 0.0;
        if (x <= 0.7)
            y = 1.0;
        else if (x > 1.4)
            y = Math.exp(a * x) * Math.cos(b * x);
        else
            y = a * Math.pow(x, 2) * Math.log(x);

        return y;
    }

    public int numberOfSteps()
    {
        return (int) ((max_X - min_X) / stepIncrement);
    }


    public double[][] calculateMatrix()
    {
        int maxSteps = numberOfSteps();
        elementArray = new double[maxSteps][3];
        double x = min_X;
        for (int step = 0; step < maxSteps; step++)
        {
            double y = function(x);
            elementArray[step][0] = step;
            elementArray[step][1] = y;
            elementArray[step][2] = x;
            x+= stepIncrement;
        }
        System.out.println(elementArray.length);
        System.out.println(elementArray[0].length);
        return elementArray;
    }

    private double[][] sortArray()
    {
        double[][] tempArray = Arrays.copyOf(elementArray, elementArray.length);

        Arrays.sort(tempArray, Comparator.comparing(y -> y[1]));
        return tempArray;
    }

    public int getIdOfHighestY()
    {
        double[][] sortedArray = sortArray();
        return (int) sortedArray[sortedArray.length-1][0];
    }

    public int getIdOfLowestY()
    {
        double[][] sortedArray = sortArray();
        return (int) sortedArray[0][0];
    }

    public void printInfoAll()
    {
        System.out.println();

        System.out.println("Calculated matrix: ");
        for (double[] element : elementArray)
            System.out.println(getInfo(element));

        System.out.println();
    }

    public String getValuesByID(int id)
    {
        if (id >= elementArray.length)
            return "Index not found!";

        return getInfo(elementArray[id]);
    }

    public String getInfo(double[] element)
    {
        String formattedX = new DecimalFormat("#0.000").format(element[2]);
        String formattedY = new DecimalFormat("#0.00000").format(element[1]);
        return "Number in array - " + (int)(element[0] + 1) + ", x = " + formattedX + " y = " + formattedY;
    }

}

