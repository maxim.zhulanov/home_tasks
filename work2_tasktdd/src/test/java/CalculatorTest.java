import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator testObject;
    @BeforeEach
    protected void setUP(){
        testObject = new Calculator(-0.5,2);
        double[][] arr = testObject.calculateMatrix();
    }

    @Test
    public void testCalculateSteps() {
        int expected = 750;
        int result = testObject.numberOfSteps();
        assertEquals(expected,result,"Must be 750!");
    }

    @Test
    void testGetIdOfHighestY(){
        int expected = 174;
        int result = testObject.getIdOfHighestY();
        assertEquals(expected,result,"Must be 174!");
    }
    @Test
    void testGetIdOfLowestY(){
        int expected = 362;
        int result = testObject.getIdOfLowestY();
        assertEquals(expected,result,"Must be 362!");
    }

    @Test
    void testCalculateArray() {
        double[][] arr = testObject.calculateMatrix();
        boolean result = arr.length > 0;
        assertTrue(result,"Array is empty!");
    }
    @Test
    void testCalculateArrayNotNull() {
        double[][] arr = testObject.calculateMatrix();
        boolean result = Arrays.stream(arr).allMatch(Objects::nonNull);
        assertTrue(result,"Array values is null!");
    }

    @Test
    void testOutOfBoundsGetValue () {
        String expected = "Index not found!";
        String result = testObject.getValuesByID(1050);
        assertEquals(expected,result,"Must be same!");
    }

}