import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {
        String path = System.getProperty("java.class.path");
        Main main = new Main();
        main.printTree(Paths.get(path), "");
    }

    public void printTree(Path path, String mask) throws IOException {
        System.out.println(path.getFileName());
        if (path.toFile().isDirectory())
            try {
                List<Path> paths = Files.list(path)
                        .sorted(Comparator.comparing((Path p) -> !p.toFile().isDirectory()).thenComparing(Path::getFileName))
                        .collect(Collectors.toList());
                for (int i = 0; i < paths.size(); i++) {
                    System.out.printf("%s%s\u2500\u2500\u2500", mask, i == paths.size() - 1 ? "\u2514" : "\u251c");
                    printTree(paths.get(i), String.format("%s%s", mask, i == paths.size() - 1 ? "    " : "\u2502   "));
                }
            } catch (AccessDeniedException ex) {
                throw new AccessDeniedException(ex.getFile());
            }
    }
}
