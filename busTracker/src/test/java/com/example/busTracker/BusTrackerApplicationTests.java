package com.example.busTracker;

import com.example.busTracker.data.Bus;
import com.example.busTracker.data.Owner;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BusTrackerApplicationTests {

	@Test
	void testValidAgeDuringEditing() {
		Owner owner = new Owner();
		assertThrows(IllegalArgumentException.class, () -> {
			owner.setAge(15);
		});
	}

	@Test
	void testValidAgeDuringCreation() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Owner("TEST", "TEST", 15, "Нет");
		});
	}

	@Test
	void testValidNumberOfBusesByOwner() {
		Owner testOwner = new Owner("TEST1", "TEST1", 25, "Нет");
		Bus testBus = new Bus("testName", "testModel", 15, 2.0, "0000", testOwner);
		Bus testBus2 = new Bus("testName", "testModel", 15, 2.0, "0000", testOwner);
		testOwner.addBus(testBus);
		testOwner.addBus(testBus2);
		assertEquals(2, testOwner.getBuses().size());
	}

	@Test
	void testTotalIncome() {
		Owner testOwner = new Owner("TEST2", "TEST2", 30, "Есть");
		Bus testBus = new Bus("testName", "testModel", 18, 2.0, "2020", testOwner);
		Bus testBus2 = new Bus("testName", "testModel", 20, 2.0, "0101", testOwner);
		testOwner.addBus(testBus);
		testOwner.addBus(testBus2);
		assertEquals(18*28*30 + 20*28*30, testOwner.getTotalIncome());
	}

	@Test
	void testFuelConsumptionOfBus() {
		Owner testOwner = new Owner("TEST3", "TEST3", 33, "Есть");
		Bus testBus = new Bus("testName", "testModel", 18, 2.0, "2020", testOwner);
		Bus testBus2 = new Bus("testName", "testModel", 20, 2.5, "0101", testOwner);
		Bus testBus3 = new Bus("testName", "testModel", 25, 3.3, "0171", testOwner);
		assertEquals(2.0 * 3.5, testBus.getFuelConsumption());
		assertEquals(2.5 * 3.5, testBus2.getFuelConsumption());
		assertEquals(3.3 * 3.5, testBus3.getFuelConsumption());
	}
}
