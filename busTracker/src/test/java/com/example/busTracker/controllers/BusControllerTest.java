package com.example.busTracker.controllers;

import com.example.busTracker.data.Bus;
import com.example.busTracker.data.Owner;
import com.example.busTracker.services.BusService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;

@ActiveProfiles("test")
@WebMvcTest(controllers = BusController.class)
@Import({BusController.class})
class BusControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BusService busService;

    private static Bus bus;
    private static Bus bus2;
    private static List<Bus> busList;

    @BeforeAll
    public static void setup() {
        Owner testOwner = new Owner("TEST_1", "TEST_1", 25, "Нет");
        Bus bus = new Bus("testName", "testModel", 15, 2.0, "0000", testOwner);
        Bus bus2 = new Bus("testName", "testModel", 15, 2.0, "0000", testOwner);
        busList = Arrays.asList(bus, bus2);
    }

    @Test
    void getAllBuses_() throws Exception {
        when(busService.findAll()).thenReturn(busList);

        mockMvc.perform(MockMvcRequestBuilders.get("/buses"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

        verify(busService, times(1)).findAll();
    }
}