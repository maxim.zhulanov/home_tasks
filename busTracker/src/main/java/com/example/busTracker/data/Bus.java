package com.example.busTracker.data;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bus")
public class Bus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", length = 20)
    private String name;

    @Column(name = "model", length = 50)
    private String model;

    @Column(name = "numberOfSeats", length = 50)
    private int numberOfSeats;

    @Column(name = "engineVolume", length = 2)
    private double engineVolume;

    @Column(name = "transportNumber", length = 10)
    private String transportNumber;

    public Bus() {
    }

    public Bus(String name, String model, int numberOfSeats, double engineVolume, String transportNumber, Owner owner) {
        this.name = name;
        this.model = model;
        this.numberOfSeats = numberOfSeats;
        this.engineVolume = engineVolume;
        this.transportNumber = transportNumber;
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public double getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(double engineVolume) {
        this.engineVolume = engineVolume;
    }

    public String getTransportNumber() {
        return transportNumber;
    }

    public void setTransportNumber(String transportNumber) {
        this.transportNumber = transportNumber;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "owner_id", nullable = false)
    private Owner owner;

    public Owner getOwner() {
        return owner;
    }
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public double getFuelConsumption() {
        return engineVolume * 3.5;
    }
}
