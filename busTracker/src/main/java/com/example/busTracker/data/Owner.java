package com.example.busTracker.data;

import jakarta.persistence.*;

import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "owner")
public class Owner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", length = 20)
    private String name;

    @Column(name = "surname", length = 20)
    private String surname;

    @Column(name = "age", length = 2)
    private int age;

    @Column(name = "isCriminal", length = 5)
    private String isCriminal;

    public Owner() {
    }

    public Owner(String name, String surname, int age, String isCriminal) {
        this.name = name;
        this.surname = surname;
        if(age < 18) throw new IllegalArgumentException("Owner can't be a children!");
        this.age = age;
        this.isCriminal = isCriminal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age < 18) throw new IllegalArgumentException();
        this.age = age;
    }

    public String getIsCriminal() {
        return isCriminal;
    }

    public void setIsCriminal(String isCriminal) {
        this.isCriminal = isCriminal;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner", cascade = CascadeType.ALL)
    private Set<Bus> buses = new HashSet<>();

    public Set<Bus> getBuses() {
        return buses;
    }

    public void setBuses(Set<Bus> buses) {
        this.buses = buses;
    }

    public void addBus(Bus bus) {
        this.buses.add(bus);
        bus.setOwner(this);
    }

    public long getTotalIncome() {
        return buses.stream()
                .mapToLong(bus -> (long) bus.getNumberOfSeats() * 28 * 30)
                .sum();
    }

    @Override
    public String toString() {
        return name + " " + surname + ", " + age + " лет";
    }
}
