package com.example.busTracker.controllers;

import com.example.busTracker.data.Bus;
import com.example.busTracker.services.BusService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;


@Controller
public class BusController {

    private final BusService busService;

    @Autowired
    public BusController(BusService busService) {
        this.busService = busService;
    }

    @GetMapping("/buses")
    public String showBuses(Model model) {
        List<Bus> buses = busService.findAll();
        model.addAttribute("buses", buses);
        return "buses";
    }

    @PostMapping("/buses")
    public String addBus(Bus bus) {
        busService.saveBus(bus);
        return "redirect:/buses";
    }

    @GetMapping("/bus/edit/{id}")
    public String editBus(@PathVariable(value = "id") long id, Model model) {
        if(!busService.existsById(id)) {
            return "redirect:/buses";
        }
        Bus bus = busService.findById(id);
        model.addAttribute("bus", bus);
        return "edit_bus";
    }

    @PostMapping("/bus/edit/{id}")
    public String updateBus(@PathVariable("id") Long id, @Valid @ModelAttribute("bus") Bus bus, Model model) {
        busService.updateBus(id, bus);
        return "redirect:/buses";
    }

    @GetMapping("bus/delete/{id}")
    public String deleteBus(@PathVariable("id") Long id){
        busService.deleteById(id);
        return "redirect:/buses";
    }
}
