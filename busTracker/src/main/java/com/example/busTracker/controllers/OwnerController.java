package com.example.busTracker.controllers;

import com.example.busTracker.data.Owner;
import com.example.busTracker.services.OwnerService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;


@Controller
public class OwnerController {

    private final OwnerService ownerService;

    @Autowired
    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping("/main")
    public String getAll() {
        return "main";
    }

    @GetMapping("/owners")
    public String showOwners(Model model) {
        List<Owner> owners = ownerService.findAll();
        model.addAttribute("owners", owners);
        return "owners";
    }

    @PostMapping("/owners")
    public String addOwner(Owner owner) {
        ownerService.saveOwner(owner);
        return "redirect:/owners";
    }

    @GetMapping("/owner/edit/{id}")
    public String editOwner(@PathVariable(value = "id") long id, Model model) {
        if(!ownerService.existsById(id)) {
            return "redirect:/owners";
        }
        Owner owner = ownerService.findById(id);
        model.addAttribute("owner", owner);
        return "edit_owner";
    }

    @PostMapping("/owner/edit/{id}")
    public String updateOwner(@PathVariable("id") Long id, @Valid @ModelAttribute("owner") Owner owner, Model model) {
        ownerService.updateOwner(id, owner);
        return "redirect:/owners";
    }

    @GetMapping("owner/delete/{id}")
    public String deleteOwner(@PathVariable("id") Long id){
        ownerService.deleteById(id);
        return "redirect:/owners";
    }

    @GetMapping("/owner_buses/{id}")
    public String busesByOwner(@PathVariable("id") long id, Model model) {
        Owner owner = ownerService.findById(id);
        model.addAttribute("owner", owner);
        return "buses_by_owner";
    }
}

