package com.example.busTracker.services;

import java.util.List;
import java.util.Optional;

import com.example.busTracker.data.Owner;
import com.example.busTracker.repositories.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OwnerService {

    private final OwnerRepository ownerRepository;

    @Autowired
    public OwnerService(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    public Owner findById(Long id){
        return ownerRepository.getById(id);
    }

    public List<Owner> findAll(){
        return ownerRepository.findAll();
    }

    public void saveOwner(Owner owner){
        ownerRepository.save(owner);
    }

    public Optional<Owner> updateOwner(Long id, Owner owner) {
        return Optional.of(ownerRepository.findById(id))
                .map(e -> {
                    Owner own = new Owner();
                            own.setId(id);
                            own.setName(owner.getName());
                            own.setSurname(owner.getSurname());
                            own.setAge(owner.getAge());
                            own.setIsCriminal(owner.getIsCriminal());
                    return ownerRepository.save(own);
                });
    }

    public boolean existsById(Long id) {
        return ownerRepository.existsById(id);
    }

    public void deleteById(Long id){
        ownerRepository.deleteById(id);
    }
}