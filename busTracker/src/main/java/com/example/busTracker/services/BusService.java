package com.example.busTracker.services;

import com.example.busTracker.data.Bus;
import com.example.busTracker.repositories.BusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BusService {

    private final BusRepository busRepository;

    @Autowired
    public BusService(BusRepository busRepository) {
        this.busRepository = busRepository;
    }

    public Bus findById(Long id){
        return busRepository.getById(id);
    }

    public List<Bus> findAll(){
        return busRepository.findAll();
    }

    public void saveBus(Bus bus){
        bus.getOwner().addBus(bus);
        busRepository.save(bus);
    }

    public boolean existsById(Long id) {
        return busRepository.existsById(id);
    }

    public void deleteById(Long id){
        busRepository.deleteById(id);
    }

    public Optional<Bus> updateBus(Long id, Bus bus) {
        return Optional.of(busRepository.findById(id))
                .map(e -> {
                    Bus b = new Bus();
                            b.setId(id);
                            b.setName(bus.getName());
                            b.setModel(bus.getModel());
                            b.setNumberOfSeats(bus.getNumberOfSeats());
                            b.setEngineVolume(bus.getEngineVolume());
                            b.setTransportNumber(bus.getTransportNumber());
                            b.setOwner(bus.getOwner());
                    return busRepository.save(b);
                });
    }
}